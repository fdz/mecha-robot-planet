﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Move : MonoBehaviour
	{
		public Vector3 dir;
		public float speed;

		void FixedUpdate()
		{
			transform.position += dir.normalized * ( speed * Time.deltaTime );
		}
	}
}