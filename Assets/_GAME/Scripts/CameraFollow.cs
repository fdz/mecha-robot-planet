﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class CameraFollow : MonoBehaviour
	{
		public Transform target;
		public float speed=5;

		/// <summary>
		/// Start is called on the frame when a script is enabled just before
		/// any of the Update methods is called the first time.
		/// </summary>
		void Start()
		{
			transform.parent = null;
		}

		/// <summary>
		/// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
		/// </summary>
		void FixedUpdate()
		{
			if ( target != null )
			{
				Vector3 pos = transform.position + (target.position-transform.position) * (speed*Time.deltaTime);
				transform.position = new Vector3(pos.x, pos.y, transform.position.z);
			}
		}
	}// end of class
}// end of namespace