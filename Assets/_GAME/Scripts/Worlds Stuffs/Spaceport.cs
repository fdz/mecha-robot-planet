﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Spaceport : MonoBehaviour
	{
		public Transform planet;
		public float force = 30;
		[Space]
		[SerializeField] float cooldown = .5f;
		[SerializeField] float timer;

		bool turnOffGFX;
		Transform child;

		/// <summary>
		/// Start is called on the frame when a script is enabled just before
		/// any of the Update methods is called the first time.
		/// </summary>
		void Start()
		{
			child = transform.GetChild(0);
		}

		void Update()
		{
			if ( timer > 0 )
			{
				child.gameObject.SetActive(false);
				timer -= Time.deltaTime;
			}
			else
			{
				child.gameObject.SetActive(true);
				timer = 0;
			}
		}

		public void Use( )
		{
			timer = cooldown;
		}

		public bool CanBeUsed()
		{
			return timer <= 0;
		}
	}// end of class
}// end of namespace