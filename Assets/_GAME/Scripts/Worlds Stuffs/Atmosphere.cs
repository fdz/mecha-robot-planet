﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Atmosphere : MonoBehaviour
	{
		public float gravity = -9.81f;
		[SerializeField] CircleCollider2D ground;
		[SerializeField] CircleCollider2D atmosphere;
		[SerializeField] Transform _spawnPoint;

		public float groundRadius { get { return ground.radius; } }
		public float atmosphereRadius { get { return atmosphere.radius; } }
		public Vector3 spawnPoint { get { return _spawnPoint.position; } }

		void OnTriggerStay2D(Collider2D other)
		{
			if ( other.GetComponent<AtmosphereIgnore>() )
			{
				return;
			}
			var otherRb = other.GetComponent<Rigidbody2D>();
			if (otherRb != null)
			{
				var otherMovement = GetComponent<MovementManager>();
				if ( otherMovement != null && otherMovement.isJumping )
				{
					return;
				}
				float otherDistance = Vector2.Distance(other.transform.position, this.transform.position);
				if (otherDistance < atmosphereRadius )
				{
					float gravityMultiplier = 1 - (otherDistance / atmosphereRadius);
					Vector3 dir = other.transform.position - this.transform.position;
					float force = gravity * gravityMultiplier;
					otherRb.AddForce(dir * force);
					// Debug.Log( force );
				}
			}
		}
	} // end of class
} // end of namespace