﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class SatelliteBullet : MonoBehaviour
	{
		public float speed = 8;

		public List<Collider2D> ignore;

		float timerMax = 25;
		float timer;

		void Start()
		{
			var trigger = GetComponent<Collider2D>();
			if ( trigger != null )
			{
				ignore.Add(trigger);
			}
		}

		void FixedUpdate()
		{
			transform.position = transform.position + transform.right * ( speed * Time.deltaTime );
			timer += Time.deltaTime;
			if ( timer >= timerMax )
			{
				timer = 0;
				Destroy(gameObject);
			}
		}

		void OnTriggerEnter2D(Collider2D other)
		{
			if ( other.GetComponent<DestroysBullet>() == false )
			{
				return;
			}
			foreach (var item in ignore)
			{
				if ( item == other.GetComponent<Collider>() )
				{
					return;
				}
			}
			Destroy( gameObject );
			
		}
	}// end of class
}// end of namespace