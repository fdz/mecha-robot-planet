﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Spawner : MonoBehaviour
	{
		public GameObject prefab;
		public float time = 10;
		public bool setPos = true;
		public bool setRot = true;
		float timer;

		void Update()
		{
			timer += Time.deltaTime;
			if ( timer >= time )
			{
				timer = 0;
				var go = Instantiate(prefab);
				if ( setRot )
				{
					go.transform.position = transform.position;
				}
				if ( setRot )
				{
					go.transform.right = transform.right;
				}
			}
		}
	}// end of class
}// end of namespace