﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Spaceship : MonoBehaviour
	{
		public Spawner shooter;
		bool isFollowing;
		public float distance = 80;
		public float speed;
		public float radius;

		void Start()
		{
			shooter.gameObject.SetActive(false);
		}

		void FixedUpdate()
		{
			Vector3 targetPos = MovementManager.playerPos;
			targetPos.z = transform.position.z;

			bool inRange = Vector2.Distance( targetPos, transform.position ) < distance;

			if ( inRange )
			{
				isFollowing = true;
				shooter.gameObject.SetActive(true);
			}

			if ( isFollowing == false && inRange == false )
			{
				return;
			}

			if ( isFollowing && inRange  )
			{
				var axis = ( transform.position - targetPos ).normalized;
				transform.RotateAround (targetPos, transform.forward, speed * Time.deltaTime);
				
				var desiredPosition = targetPos + (transform.position - targetPos).normalized * radius;
				transform.position = Vector3.MoveTowards(transform.position, desiredPosition, speed * Time.deltaTime);
				// transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);

				Vector3 diff = targetPos - transform.position;
				float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
				Quaternion rot = Quaternion.Euler(0f, 0f, rot_z);
				transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * 10);
			}
			else if ( isFollowing )
			{
				var desiredPosition = targetPos + (transform.position - targetPos).normalized * radius;
				transform.position = Vector3.MoveTowards(transform.position, desiredPosition, speed * Time.deltaTime);

				Vector3 diff = targetPos - transform.position;
				float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
				Quaternion rot = Quaternion.Euler(0f, 0f, rot_z);
				transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * 10);
			}
		}

		void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.red;
        	Gizmos.DrawWireSphere(transform.position, distance);
		}
	}
}