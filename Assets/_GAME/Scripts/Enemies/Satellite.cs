﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Satellite : MonoBehaviour
	{
		public Atmosphere curAtmosphere;
		public float speed;
		public float radius;

		void FixedUpdate()
		{
			if ( curAtmosphere != null )
			{
				Vector3 targetPos = curAtmosphere.transform.position;

				var axis = ( transform.position - targetPos ).normalized;
				transform.RotateAround (targetPos, transform.forward, speed * Time.deltaTime);
				
				var desiredPosition = targetPos + (transform.position - targetPos).normalized * radius;
				transform.position = Vector3.MoveTowards(transform.position, desiredPosition, speed * Time.deltaTime);
			}
			// transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
		}
	}
}