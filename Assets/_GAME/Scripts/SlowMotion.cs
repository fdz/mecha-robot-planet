﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class SlowMotion : MonoBehaviour
	{
		// https://www.youtube.com/watch?v=0VGosgaoTsw
		[Range(0, 1)] public float amount = .5f;
		bool slowMotionActive;

		const float SPEED = 5;

		Vector3 gravity;
		
		void Start()
		{
			gravity = Physics.gravity;
		}

		void Update()
		{
			if ( Input.GetKeyDown( KeyCode.LeftControl ) )
			{
				slowMotionActive = !slowMotionActive;
			}

			Tick();
		}

		void Tick()
		{
			float time = Time.deltaTime * SPEED;
			if ( slowMotionActive )
			{
				Time.timeScale = amount;
				Time.fixedDeltaTime = Time.timeScale * .02f;
			}
			else
			{
				Time.timeScale = 1;
				Time.fixedDeltaTime = Time.timeScale * .02f;
			}
		}
	}// end of class
}// end of namespace