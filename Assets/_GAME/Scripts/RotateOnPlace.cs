﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class RotateOnPlace : MonoBehaviour
	{
		public float speed;
		Vector3 axis;

		void Start()
		{
			axis = new Vector3(Random.value, Random.value, Random.value).normalized;
		}

		void Update()
		{
        	transform.RotateAround(transform.position, axis, speed * Time.deltaTime);
		}
	}
}