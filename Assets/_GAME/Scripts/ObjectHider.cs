﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class ObjectHider : MonoBehaviour
	{
		public bool visbleGroupA = true;
		public Transform[] groupA;
		public Transform[] groupB;

		void Awake()
		{
			SetVisible(visbleGroupA);
		}
		
		void Update()
		{
			if ( Input.GetKeyDown( KeyCode.F1 ) )
			{	
				visbleGroupA = !visbleGroupA;
				SetVisible(visbleGroupA);
			}
		}

		void SetVisible( bool value )
		{
			foreach (var item in groupA)
			{
				item.gameObject.SetActive( value );
			}
			foreach (var item in groupB)
			{
				item.gameObject.SetActive( !value );
			}
		}
	}//
}//