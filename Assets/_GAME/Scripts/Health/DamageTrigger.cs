﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class DamageTrigger : MonoBehaviour
	{
		public string damageTag = "Player";
		public bool damageActive;
		public int damageValue = 1;
		public Collider2D[] ignore;

		void OnTriggerEnter2D(Collider2D other)
		{
			if ( damageActive == false || other.tag != damageTag )
			{
				// Debug.Log("no damage to " + other.name);
				return;
			}

			foreach (var item in ignore)
			{
				if (item == other)
				{
					return;
				}
			}

			var otherHealth = other.GetComponent<Health>();
			if (otherHealth != null)
			{
				otherHealth.TakeDamage(damageValue);
			}
		}
	} //end of class
} // end of namesapce