﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Health : MonoBehaviour
	{
		[SerializeField] int _valueMax = 1;
		[SerializeField] GameObject deathPrefab;
		[SerializeField] float deathPrefabScale = 1;
		// [SerializeField] bool onHitInvencible;
		// [SerializeField] float onHitInvTime;
		[SerializeField] bool regen;
		[SerializeField] int regenValue = 1;
		[SerializeField] float regenTime = 1;
		float regenTimer;
		int _value = 1;
		bool dead;
		bool invencible;
		float invencibleTimerMax;
		float invencibleTimer;

		public int value { get { return _value; } protected set { _value = value; } }
		public int valueMax { get { return _valueMax; } protected set { _valueMax = value; } }
		public bool isDead { get { return dead; } }
		public bool isInvencible { get { return invencible; } }

		void Start()
		{
			_value = _valueMax;
		}

		public void TakeDamage(int damageValue)
		{
			if (invencible)
			{
				return;
			}

			value -= Mathf.Abs(damageValue);
			value = Mathf.Clamp(value, 0, valueMax);
			Debug.Log( this.name + " took damage, health = " + value + "/" + valueMax);

			if (value <= 0)
			{
				Die();
			}
		}

		public void Heal(int healValue)
		{
			if (dead)
			{
				return;
			}

			value += Mathf.Abs(healValue);
			value = Mathf.Clamp(value, 0, valueMax);

			if (value <= 0)
			{
				Die();
			}
		}

		public void MakeInvencible(float time)
		{
			invencible = true;
			invencibleTimerMax += Mathf.Abs(time);
		}

		void Die()
		{
			dead = true;
			invencible = false;
			var go = Instantiate(deathPrefab);
			go.transform.position = transform.position;
			go.transform.localScale = go.transform.localScale * deathPrefabScale;

			if ( tag == "Enemy" )
			{
				MovementManager.EnemyKilled();
				transform.gameObject.SetActive(false);
			}
			else if ( tag == "Player" )
			{
				value = valueMax;
				MovementManager.Respawn();
			}
		}

		void Update()
		{
			if (dead)
			{
				return;
			}

			if (invencible)
			{
				invencibleTimer += Time.deltaTime;
				if (invencibleTimer >= invencibleTimerMax)
				{
					invencibleTimer = 0;
					invencibleTimerMax = 0;
					invencible = false;
				}
			}

			if ( regen && value < valueMax )
			{
				regenTimer += Time.deltaTime;
				if (regenTimer >= regenTime)
				{
					regenTimer = 0;
					Heal(regenValue);
				}
			}
			else
			{
				regenTimer = 0;
			}
		}
	} // end of class
} //end of namespace