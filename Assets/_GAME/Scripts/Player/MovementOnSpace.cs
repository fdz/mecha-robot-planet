﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class MovementOnSpace : MonoBehaviour
	{
		[SerializeField] float rotSpeed = 4;
		[SerializeField] float moveForce = 300;
		[SerializeField] float drag = 5;
		[SerializeField] Transform shooter;
		[SerializeField] GameObject projectile;
		[SerializeField] float shootTime;
		float shootTimer;

		// SCENE REFERENCES
		MovementAux aux;
		MovementInput input;
		Rigidbody2D rb;
		CircleCollider2D coll;
		Animator anim;
		Transform gfx;

		public void Init(MovementAux aux, MovementInput input, Rigidbody2D rb, CircleCollider2D coll, Animator anim, Transform gfx)
		{
			this.aux = aux;
			this.input = input;
			this.rb = rb;
			this.coll = coll;
			this.anim = anim;
			this.gfx = gfx;
		}

		public void FixedTick()
		{
			rb.drag = drag;
			Move();
			RotateRigidbody();
			RotateGFX();

			shootTimer -= Time.deltaTime;

			if ( input.dash )
			{
				input.dash = false;
				if ( shootTimer <= 0 )
				{
					shootTimer = shootTime;
					var go = Instantiate(projectile);
					go.transform.position = shooter.position;

					Vector3 diff = input.mousePosition - go.transform.position;
					float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
					Quaternion rot = Quaternion.Euler(0f, 0f, rot_z);
					go.transform.rotation = rot;
					// go.transform.right = shooter.right;
				}
			}
		}

		void Move()
		{
			if ( Input.GetMouseButton(1) )
			{
				rb.AddForce( (-transform.right) * (moveForce * input.GetMoveAmount() ) );
			}
			else
			{
				rb.AddForce( transform.right * (moveForce * input.GetMoveAmount() ) );
			}
		}

		void RotateRigidbody()
		{
			Vector3 diff = input.mousePosition - transform.position;
			float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
			Quaternion rot = Quaternion.Euler(0f, 0f, rot_z);
			transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * rotSpeed);
		}

		void RotateGFX()
		{
			gfx.localRotation = Quaternion.Lerp(gfx.localRotation, Quaternion.Euler(0, 0, -90), Time.deltaTime * 12);
		}
	} // end of class
} // end of namespace