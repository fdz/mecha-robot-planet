﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class PlayerAnimHook : MonoBehaviour
	{
		[SerializeField] DamageTrigger dmgTrigger;
		void OpenDamageTriggers()
		{
			dmgTrigger.damageActive = true;
		}
		void CloseDamageTriggers()
		{
			dmgTrigger.damageActive = false;
		}
	}
}