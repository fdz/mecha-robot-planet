﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	[System.Serializable]
	public class MovementAux
	{
		[SerializeField] int dashCost = 2;
		[Space]
		[SerializeField] int _curStamina = 6;
		[SerializeField] int _maxStamaina = 6;
		[SerializeField] int staminaRocovered = 1;
		[SerializeField] float staminaRecoviryTime = 1.5f;
		float staminaTimer;

		public int curStamina { get { return _curStamina; } protected set { _curStamina = value; } }
		public int maxStamina { get { return _maxStamaina; } protected set { _maxStamaina = value; } }
		
		public bool HasStaminaForDash()
		{
			return curStamina >= dashCost;
		}

		public void EnemyKilled()
		{
			curStamina += dashCost;
			curStamina = Mathf.Clamp(curStamina, 0, maxStamina);
		}

		public void RachargeFullStamina()
		{
			curStamina = maxStamina;
		}

		public void ConsumeStaminaForDash()
		{
			curStamina -= dashCost;
			curStamina = Mathf.Clamp(curStamina, 0, maxStamina);
		}

		public void StaminaRecoveryTick()
		{
			if (curStamina < maxStamina)
			{
				staminaTimer += Time.deltaTime;
				if (staminaTimer > staminaRecoviryTime)
				{
					staminaTimer = 0;
					curStamina += staminaRocovered;
				}
			}
			else
			{
				staminaTimer = 0;
			}
			
			curStamina = Mathf.Clamp(curStamina, 0, maxStamina);
		}
	} //end of class
} //end of namesapce