﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FDZ
{
	public class MovementManager : MonoBehaviour
	{
		static MovementManager ins;

		public static Vector3 playerPos { get { return ins == null ? Vector3.zero : ins.transform.position; } }

		public static void EnemyKilled()
		{
			ins.aux.RachargeFullStamina();
		}

		public static void Respawn()
		{
			ins.aux.RachargeFullStamina();
			if ( ins.hasSpawnPoint )
			{
				ins.transform.position = ins.spawnPoint;
			}
		}

		enum ActiveModeType { OnPlanet, OnSpace }

		[SerializeField] ActiveModeType activeMode;
		[SerializeField] Collider2D triggerOnPlanet;
		[SerializeField] Collider2D triggerOnSpace;
		[SerializeField] DamageTrigger swordDmgTrigger;
		[SerializeField] MovementAux aux;
		[SerializeField] MovementInput input;
		[Space]
		[SerializeField] Transform gfx;
		[SerializeField] Transform swordGfx;
		[SerializeField] Transform arrowGfx;
		[SerializeField] Transform circlesGfx;
		[SerializeField] Image healthBar;
		[SerializeField] Image dashBar;
		[SerializeField] MeleeWeaponTrail dashTrail;

		bool hasSpawnPoint;
		Vector3 spawnPoint;

		Transform arrowGfxChild;

		MovementOnPlanet onPlanet;
		MovementOnSpace onSpace;

		const float GRAVITY_SCALE = 0;
		const float ANGULAR_DRAG = 9f;
		// SCENE REFERENCES
		Rigidbody2D rb;
		CircleCollider2D coll;
		Animator anim;
		Health health;

		[Header("DEBUG")]
		[SerializeField] float dbg_rbVelMag;

		public bool isJumping { get { return onPlanet.isJumping; } }

		void Awake()
		{
			ins = this;

			rb = GetComponent<Rigidbody2D>();
			coll = GetComponent<CircleCollider2D>();
			anim = GetComponentInChildren<Animator>();
			health = GetComponent<Health>();

			rb.constraints = RigidbodyConstraints2D.FreezeRotation;
			rb.gravityScale = GRAVITY_SCALE;
			rb.angularDrag = ANGULAR_DRAG;

			input.Init(transform);

			onPlanet = GetComponent<MovementOnPlanet>();
			onPlanet.Init(aux, input, rb, coll, anim, gfx);

			onSpace = GetComponent<MovementOnSpace>();
			onSpace.Init(aux, input, rb, coll, anim, gfx);

			arrowGfxChild = arrowGfx.GetChild(0);

			// Cursor.visible = false;
			circlesGfx.gameObject.SetActive(true);
		}

		bool IsOnPlanet()
		{
			return onPlanet.onPlanet;
		}
		
		void Update()
		{
			input.Tick();
		}

		void FixedUpdate()
		{
			dbg_rbVelMag = rb.velocity.magnitude;

			if (IsOnPlanet())
			{
				hasSpawnPoint = true;
				spawnPoint = onPlanet.curPlanetSpawnPoint;

				anim.SetBool("onSpace", false);
				activeMode = ActiveModeType.OnPlanet;
				triggerOnPlanet.gameObject.SetActive(true);
				triggerOnSpace.gameObject.SetActive(false);
				swordGfx.gameObject.SetActive(true);

				onPlanet.FixedTick();
			}
			else
			{
				anim.SetBool("onSpace", true);
				activeMode = ActiveModeType.OnSpace;
				triggerOnPlanet.gameObject.SetActive(false);
				triggerOnSpace.gameObject.SetActive(true);
				swordGfx.gameObject.SetActive(false);
				swordDmgTrigger.damageActive = false;

				onSpace.FixedTick();
			}

			aux.StaminaRecoveryTick();
		}

		void LateUpdate()
		{
			GFXDashTrail();
			GFXArrowTick();
			GFXStaminaBarTick();
			GFXHealthBarTick();
		}

		void GFXDashTrail()
		{
			if (onPlanet.isDashing && IsOnPlanet())
			{
				dashTrail.lifeTime = .15f;
			}
			else
			{
				dashTrail.lifeTime = Mathf.Lerp(dashTrail.lifeTime, 0, Time.deltaTime * 4);
			}
		}

		void GFXArrowTick()
		{
			Vector3 diff = input.mousePosition - transform.position;
			float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
			Quaternion rot = Quaternion.Euler(0f, 0f, rot_z);
			arrowGfx.transform.rotation = rot;
			arrowGfx.transform.position = transform.position;
			arrowGfxChild.transform.localPosition = new Vector3(15, 0, 0);
		}

		void GFXStaminaBarTick()
		{
			float f = ((float) (aux.curStamina) / (float) (aux.maxStamina));
			var rectTrans = dashBar.GetComponent<RectTransform>();
			// rectTrans.sizeDelta = Vector2.Lerp( rectTrans.sizeDelta, new Vector2( 5, f * 100 ), Time.deltaTime * 10 );
			rectTrans.sizeDelta = new Vector2(5, f * 100);
		}

		void GFXHealthBarTick()
		{
			float f = ((float) (health.value) / (float) (health.valueMax));
			var rectTrans = healthBar.GetComponent<RectTransform>();
			// rectTrans.sizeDelta = Vector2.Lerp( rectTrans.sizeDelta, new Vector2( 5, f * 100 ), Time.deltaTime * 10 );
			rectTrans.sizeDelta = new Vector2(5, f * 100);
		}
	} //end of class
} //end of namespace