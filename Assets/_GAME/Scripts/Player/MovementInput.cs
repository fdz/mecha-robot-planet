﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FDZ
{
	[System.Serializable]
	public class MovementInput
	{
		// public bool spaceportLaunch;
		// public Vector3 moveDir;

		Transform owner;

		public Vector3 moveDir;
		public Vector3 mousePosition;
		public Vector3 dirToMouse;
		public float distanceToMouse;
		public bool jump;
		public bool dash;

		public void Init(Transform owner)
		{
			this.owner = owner;
		}

		public void Tick()
		{
			// float horizontal = Input.GetAxisRaw( "Horizontal" );
			// float vertical = Input.GetAxisRaw( "Vertical" );
			// moveDir = ( Vector3.right * horizontal + Vector3.up * vertical ).normalized;

			mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePosition.z = owner.position.z;
			distanceToMouse = Vector2.Distance( owner.transform.position, mousePosition );
			dirToMouse = ( mousePosition - owner.position ).normalized;

			if (Input.GetMouseButton(1))
			{
				moveDir = dirToMouse;
			}
			else
			{
				moveDir = Vector3.zero;
			}
			
			if (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Space) )
			{
				jump = true;
			}
			
			if (Input.GetMouseButtonDown(0))
			{
				dash = true;
			}

			if ( Input.GetKeyDown(KeyCode.Escape) )
			{
				Application.Quit();
			}

			if ( Input.GetKeyDown(KeyCode.R) )
			{
				SceneManager.LoadScene( 0 );
			}
		}

		public float GetMoveAmount()
		{
			float distance = Mathf.Clamp(distanceToMouse, 0, 30);
			float amount = distance / 30;
			// float amount = distance / maxDis;
			if (distance < 10)
			{
				amount = 0;
			}
			// Debug.Log(distance);
			return amount;
		}
	}// end of class
}// end of namespace