﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class MovementOnPlanet : MonoBehaviour
	{
		const float MAX_DIS_TO_TARGET = 2f;
		const float OGROUND_AUXDIS = 1;

		// const float DRAG_ONPLANET_MOVING = 5f;
		// const float DRAG_ONPLANET_IDLE = 10f;

		// const float DRAG_ONSPACE_MOVING = 5f;
		// const float DRAG_ONSPACE_IDLE = 1f;

		// VARIABLES
		public AnimationCurve moveAngleCurve = new AnimationCurve(
			new Keyframe(0, 0),
			new Keyframe(15, .1f),
			new Keyframe(30, .1f),
			new Keyframe(45, .1f),
			new Keyframe(90, 1)
		);
		[Space]
		[SerializeField] float minDis = 10;
		[SerializeField] float maxDis = 30;
		[Space]
		[SerializeField] float rotSpeed = 8;
		[Space]
		[SerializeField] float keepGroundedForce = 30;
		[SerializeField] float extraGravity = 0;
		[SerializeField] float curExtraGravity = 0;
		[Space]
		[SerializeField] float moveForceGround = 200;
		[SerializeField] float moveForceOnAir = 200;
		[SerializeField] float drag = 5;
		[Space]
		[SerializeField] bool canDash;
		[SerializeField] bool _isDashing;
		[SerializeField] float dashForce = 300;
		[SerializeField] float dashForceUseRate = 0;
		[SerializeField] float curDashForce = 0;
		[Space]
		[SerializeField] bool canJump;
		[SerializeField] bool _isJumping;
		[Space]
		[SerializeField] float normalJumpForce = 15;
		[SerializeField] float normalJumpForceUseRate = 2;
		[Space]
		[SerializeField] float spaceportJumpForce = 15;
		[SerializeField] float spaceportJumpForceUseRate = 2;
		[Space]
		[SerializeField] float curJumpForce;
		[SerializeField] float curJumpForceUseRate;
		[Space]
		[SerializeField] float jumpDistanceToPlanet;
		[SerializeField] float jumpRelativeDistanceToPlanet;
		[Space]
		[SerializeField] float curAnimMoveAmount;

		int facing = 1;
		bool faceToMouse = true;
		int dashDir;

		Vector3 curJumpDir;
		Vector3 relativeVelocity;
		Vector3 prevPos;

		Atmosphere curPlanet;
		Vector3 curPlanetDifference;
		Vector3 curPlanetDirection;
		float curPlanetDistance;

		[Header("READ ONLY")]
		[SerializeField] bool _onPlanet;
		[SerializeField] bool wasOnGround;
		[SerializeField] bool onGround;
		[SerializeField] bool isLanding;
		[SerializeField] float groundOffMaxTime;
		[SerializeField] float groundOffTimer;

		Vector3 curMoveDir;
		Spaceport curSpaceport;

		public bool onPlanet { get { return _onPlanet; } protected set { _onPlanet = value; } }
		public bool isJumping { get { return _isJumping; } protected set { _isJumping = value; } }
		public bool isDashing { get { return _isDashing; } protected set { _isDashing = value; } }
		public Vector3 curPlanetSpawnPoint { get { return curPlanet.spawnPoint; } }

		// SCENE REFERENCES
		MovementAux aux;
		MovementInput input;
		Rigidbody2D rb;
		CircleCollider2D coll;
		Animator anim;
		Transform gfx;

		public void Init(MovementAux aux, MovementInput input, Rigidbody2D rb, CircleCollider2D coll, Animator anim, Transform gfx)
		{
			this.aux = aux;
			this.input = input;
			this.rb = rb;
			this.coll = coll;
			this.anim = anim;
			this.gfx = gfx;
		}

		void OnTriggerEnter2D(Collider2D other)
		{
			var spaceport = other.GetComponent<Spaceport>();
			if (spaceport != null && spaceport.CanBeUsed())
			{
				StopJump();
				curSpaceport = spaceport;
				return;
			}
		}

		void OnTriggerStay2D(Collider2D other)
		{
			var atmosphere = other.GetComponent<Atmosphere>();
			if (atmosphere != null)
			{
				SetCurPlanet(atmosphere);
				return;
			}
			var spaceport = other.GetComponent<Spaceport>();
			if (spaceport != null && spaceport.CanBeUsed())
			{
				curSpaceport = spaceport;
				return;
			}
		}

		void OnTriggerExit2D(Collider2D other)
		{
			var spaceport = other.GetComponent<Spaceport>();
			if (spaceport != null)
			{
				StopJump();
				curSpaceport = null;
				return;
			}
		}

		bool SetCurPlanet( Atmosphere planet )
		{
			onPlanet = true;
			curPlanet = planet;

			curPlanetDifference = transform.position - curPlanet.transform.position;
			curPlanetDirection = curPlanetDifference.normalized;
			curPlanetDistance = Vector2.Distance(curPlanet.transform.position, transform.position);

			if (curPlanetDistance > curPlanet.atmosphereRadius)
			{
				// Debug.Log("not on planet | dis: " + curPlanetDistance + ", radius: " + curPlanet.atmosphereRadius);
				onPlanet = false;
				curPlanet = null;
				return false;
			}
			return true;
		}

		public void FixedTick()
		{
			if ( curPlanet == null || SetCurPlanet(curPlanet) == false )
			{
				return;
			}

			// distanceToPlanet = Vector2.Distance( curPlanet.transform.position, transform.position );
			// relativeDistanceToPlanet = distanceToPlanet / curPlanet.atmosphereRadius;
			jumpDistanceToPlanet = curPlanetDistance - coll.radius - curPlanet.groundRadius;
			jumpDistanceToPlanet = Mathf.Clamp(jumpDistanceToPlanet, 0, curPlanet.atmosphereRadius);
			float div = curPlanet.atmosphereRadius - curPlanet.groundRadius;
			jumpRelativeDistanceToPlanet = 1 - jumpDistanceToPlanet / div;
			if (jumpRelativeDistanceToPlanet < 0)
			{
				Debug.LogError("jumpRelativeDistanceToPlanet is < 0");
			}
			// Debug.Log("dis: " + distanceToPlanet + ", div: " + div + ", r: " + relativeDistanceToPlanet);

			rb.drag = drag;
			relativeVelocity = transform.InverseTransformDirection(rb.velocity);

			RotateRigidbody();
			GroundCHeck();
			Move();
			Jump();
			Dash();

			if (isJumping == false && onGround == false)
			{
				curExtraGravity = Mathf.Lerp(curExtraGravity, extraGravity, Time.deltaTime * .25f);
				rb.AddForce(curPlanetDirection * curExtraGravity);
				// Debug.DrawRay(transform.position + transform.right, curPlanetDirection * 200, Color.cyan);
			}
			else
			{
				// curExtraGravity = Mathf.Lerp(curExtraGravity, 0, Time.deltaTime * 8);
				curExtraGravity = 0;
				if (onGround)
				{
					rb.AddForce(curPlanetDirection * keepGroundedForce);
					// Debug.DrawRay( transform.position + transform.right, curPlanetDirection * 200, Color.cyan );
				}
			}

			// anim.SetFloat("moveAmount", curAnimMoveAmount);
			float disToPrevPos = Vector3.Distance(transform.position, prevPos);
			float mAmount =  disToPrevPos / .3f;
			// Debug.Log(distance);

			anim.SetFloat("moveAmount", mAmount );

			anim.SetBool("onSpace", !onPlanet);
			anim.SetBool("onGround", onGround);
			isLanding = anim.GetBool("isLanding");
			RotateGFX();

			prevPos = transform.position;

			// Debug.DrawRay(transform.position, transform.right.normalized * 8, Color.red);
			// Debug.DrawRay(transform.position, curPlanetDirection.normalized * 40, Color.magenta);

		}

		Vector3 GetMoveDir()
		{
			float angleRight = 90 - Vector3.Angle(input.dirToMouse, curPlanetDirection);
			float angleRightSign = System.Math.Sign(angleRight);
			Vector3 up = transform.right * angleRightSign;

			// Debug.DrawRay( transform.position, up * 8, Color.green );
			// Debug.Log(angleRightSign);

			float angleUp = 90 - Vector3.Angle(input.dirToMouse, transform.up);
			float anlgeUpSign = System.Math.Sign(angleUp);
			Vector3 right = transform.up * anlgeUpSign;
			// Debug.Log(angleUp);

			// Debug.DrawRay( transform.position, input.dirToMouse * 8, Color.red );
			// Debug.DrawRay( transform.position, faceDir * 8, Color.magenta );

			Vector3 vec1 = input.dirToMouse * input.distanceToMouse;
			Vector3 vec2 = right * input.distanceToMouse;

			// Debug.DrawRay( transform.position, vec1, Color.red );
			// Debug.DrawRay( transform.position, vec2, Color.magenta );

			float axis = Vector3.Angle(vec1, vec2);
			// Debug.Log(axis);

			Vector3 rotatedDirection = Quaternion.Euler(0, 0, axis * anlgeUpSign * angleRightSign) * vec1;
			rotatedDirection.Normalize();

			Debug.DrawRay(transform.position, input.dirToMouse * 8, Color.red);
			Debug.DrawRay(transform.position, rotatedDirection * 8, Color.magenta);

			// Debug.DrawRay(transform.position, input.dirToMouse * input.distanceToMouse, Color.red);
			// Debug.DrawRay(transform.position, rotatedDirection * input.distanceToMouse, Color.magenta);

			return rotatedDirection;
		}

		void Dash()
		{
			if (onGround == false)
			{
				canDash = aux.HasStaminaForDash();
			}
			else
			{
				canDash = false;
			}

			if (canDash && input.dash && isDashing == false)
			{
				canDash = false;
				isDashing = true;
				curDashForce = dashForce;
				dashDir = facing;
				rb.velocity = Vector3.zero;
				aux.ConsumeStaminaForDash();
				anim.CrossFade("sword swing", .15f);
			}

			if (isDashing)
			{
				curDashForce -= dashForceUseRate * Time.deltaTime;
				// rb.AddForce(transform.up * ( curDashForce * dashDir ) );
				rb.AddForce(input.dirToMouse * (curDashForce));
				if (curDashForce <= 0 || onGround)
				{
					canDash = true;
					isDashing = false;
					curDashForce = 0;
				}
			}

			input.dash = false;
		}

		void PauseGroundCheck()
		{
			groundOffTimer = groundOffMaxTime;
		}

		void GroundCHeck()
		{
			wasOnGround = onGround;
			if (onPlanet)
			{
				if (groundOffTimer >= 0)
				{
					groundOffTimer -= Time.deltaTime;
					return;
				}

				var groundDistance = Vector2.Distance(transform.position, curPlanet.transform.position);
				if (groundDistance < curPlanet.groundRadius + OGROUND_AUXDIS)
				{
					onGround = true;
				}
				else
				{
					onGround = false;
				}
			}
			else
			{
				onGround = false;
			}
		}

		void Move()
		{
			// float angle = input.moveDir.magnitude == 0 ? 0 : (90 - Vector3.Angle(input.moveDir, transform.up));
			float angle = (90 - Vector3.Angle(input.dirToMouse, transform.up));
			float angleRight = input.moveDir.magnitude == 0 ? 0 : (90 - Vector3.Angle(input.moveDir, curPlanetDirection));
			// Debug.Log(angleRight);
			// Debug.Log(angle);

			if (wasOnGround == false && onGround)
			{
				curMoveDir = Vector3.zero;
			}

			// if (isLanding == false && input.moveDir.magnitude != 0 && Mathf.Abs(angle) > 15)
			// Debug.Log(angle);
			if (isLanding == false)
			// if (isLanding == false )
			{
				curAnimMoveAmount = Mathf.Lerp(curAnimMoveAmount, 1, Time.deltaTime * 3.5f);

				if (onGround)
				{
					// curMoveDir = input.moveDir;
					// curMoveDir = Vector3.ProjectOnPlane(input.moveDir, transform.right);
					curMoveDir = GetMoveDir();
					curMoveDir.Normalize();
					rb.AddForce(curMoveDir * (moveForceGround * input.GetMoveAmount()));
				}
				else if (Mathf.Abs(angle) > 15)
				{
					// curMoveDir = transform.up * System.Math.Sign(angle);
					curMoveDir = GetMoveDir();
					// curMoveDir = input.moveDir;
					curMoveDir.Normalize();
					rb.AddForce(curMoveDir * (moveForceOnAir * input.GetMoveAmount()));
					// Debug.Log( "angle: " + angle + ", mod: " + moveAngleCurve.Evaluate( Mathf.Abs(angle) ) );
					// rb.AddForce(curMoveDir * (curMoveForce * moveAngleCurve.Evaluate( Mathf.Abs(angle) ))  );
				}
			}
			else
			{
				curAnimMoveAmount = Mathf.Lerp(curAnimMoveAmount, 0, Time.deltaTime * 8);
			}

			// Debug.DrawRay(transform.position, input.moveDir * 8, Color.red);
			// Debug.DrawRay(transform.position, curMoveDir * 8, Color.cyan);
			// Debug.DrawRay(transform.position, rb.velocity.normalized * 8, Color.yellow);
		}

		void StopJump()
		{
			canJump = true;
		}

		void Jump()
		{
			if (onGround)
			{
				canJump = true;
			}

			// if (onGround && input.jump && isJumping == false)
			// if (canJump && input.jump && isJumping == false)
			if (canJump && input.jump)
			{
				canJump = false;
				onGround = false;
				input.jump = false;
				isJumping = true;
				rb.velocity = Vector3.zero;
				anim.SetTrigger("jump");
				PauseGroundCheck();

				// if ( curSpaceport != null && curSpaceport.CanBeUsed() )
				if (curSpaceport != null)
				{
					// Debug.Log("spaceport");
					curJumpForce = Mathf.Abs(curSpaceport.force);
					// curJumpForceUseRate = Mathf.Abs(curSpaceport.force / 3);
					curJumpForceUseRate = Mathf.Abs(curJumpForce * (7 / 6));
					curJumpDir = curPlanetDirection;
					curSpaceport.Use();
					curSpaceport = null;

					spaceportJumpForce = curJumpForce;
					spaceportJumpForceUseRate = curJumpForceUseRate;

					// aux.RechargeStaminaOnSpaceport();
				}
				else
				{
					// Debug.Log("jump");
					curJumpForce = Mathf.Abs(normalJumpForce);
					curJumpForceUseRate = Mathf.Abs(normalJumpForceUseRate);
					curJumpDir = curPlanetDirection;
					// curJumpDir = input.moveDir.normalized;
				}
			}

			if (isJumping)
			{
				curJumpForce -= curJumpForceUseRate * Time.deltaTime;

				if (curJumpForce <= 0 || onGround)
				{
					curSpaceport = null;
					isJumping = false;
					curJumpForce = 0;
					// curJumpForceUseRate = 0;
					// Debug.Log("jumping stopped");
				}
				else
				{
					// rb.AddForce(curJumpDir * curJumpForce * jumpRelativeDistanceToPlanet);
					rb.AddForce(curJumpDir * curJumpForce );
					// Debug.DrawRay(transform.position + transform.right, curJumpDir * 200, Color.white);
				}
			}

			input.jump = false;
		}

		void RotateRigidbody()
		{
			Vector3 diff = curPlanetDifference;
			float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
			Quaternion rot = Quaternion.Euler(0, 0, rot_z);
			transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * rotSpeed);
		}

		void RotateGFX()
		{
			float angle = 90 - Vector3.Angle(input.dirToMouse, transform.up);

			if (Mathf.Abs(angle) > 10)
			{
				facing = System.Math.Sign(angle);
			}

			if (facing < 0)
			{
				gfx.localRotation = Quaternion.Lerp(gfx.localRotation, Quaternion.Euler(180, 0, -90), Time.deltaTime * 8);
			}
			else if (facing > 0)
			{
				gfx.localRotation = Quaternion.Lerp(gfx.localRotation, Quaternion.Euler(0, 0, -90), Time.deltaTime * 8);
			}
		}
	} //end of class
} //end of namespace