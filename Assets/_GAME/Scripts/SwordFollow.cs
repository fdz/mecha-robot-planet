﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class SwordFollow : MonoBehaviour
	{
		public Transform target;
		public Transform lookAt;

		void FixedUpdate()
		{
			Follow();
		}

		void LateUpdate()
		{
			Follow();
		}

		void Follow()
		{
			transform.position = target.position;

			Vector3 diff = lookAt.position - target.position;
			float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
			Quaternion rot = Quaternion.Euler(0f, 0f, rot_z);

			transform.rotation = rot;
		}
	}// end of class
}// end of namespace