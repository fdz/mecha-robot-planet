﻿using System.Collections;
using ProtoTurtle.BitmapDrawing;
using UnityEngine;

public class Example : MonoBehaviour
{
        void Start()
        {
                // Material material = GetComponent<Renderer>().material;
                // Texture2D texture = new Texture2D(512, 512, TextureFormat.RGB24, false);
                // texture.wrapMode = TextureWrapMode.Clamp;
                // material.SetTexture(0, texture);
                // texture.DrawFilledRectangle(new Rect(0, 0, 120, 120), Color.green);
                // texture.DrawRectangle(new Rect(0, 0, 120, 60), Color.red);
                // texture.DrawCircle(256, 256, 100, Color.cyan);
                // texture.DrawFilledCircle(256, 256, 50, Color.grey);
                // texture.DrawCircle(0, 0, 512, Color.red);
                // texture.DrawLine(new Vector2(120, 60), new Vector2(256, 256), Color.black);
                // texture.Apply();

                // Texture2D texture = new Texture2D(512, 512, TextureFormat.RGB24, false);
                Texture2D texture = new Texture2D(1024, 1024, TextureFormat.ARGB4444, false);
                this.GetComponent<MeshRenderer>().material.mainTexture = texture;
                texture.wrapMode = TextureWrapMode.Clamp;
                Color transparent = new Color(0, 0, 0, 0);
                for (int i = 0; i < 1024; i++)
                {
                        for (int j = 0; j < 1024; j++)
                        {
                                texture.SetPixel(i, j, transparent);
                        }
                }
                // texture.DrawFilledRectangle(new Rect(0, 0, 120, 120), Color.green);
                // texture.DrawRectangle(new Rect(0, 0, 120, 60), Color.red);
                // texture.DrawCircle(256, 256, 100, Color.cyan);
                // texture.DrawFilledCircle(256, 256, 50, Color.grey);

                Color color = new Color(.3f, .3f, .3f, .05f);

                texture.DrawCircle(512, 512, 512, color);
                texture.DrawCircle(512, 512, 511, color);
                texture.DrawCircle(512, 512, 510, color);
                texture.DrawCircle(512, 512, 509, color);
                texture.DrawCircle(512, 512, 508, color);

                texture.DrawCircle(512, 512, 200, color);
                texture.DrawCircle(512, 512, 199, color);
                texture.DrawCircle(512, 512, 198, color);
                texture.DrawCircle(512, 512, 197, color);
                texture.DrawCircle(512, 512, 196, color);

                // texture.DrawLine(new Vector2(120, 60), new Vector2(256, 256), Color.black);
                texture.Apply();
        }
}